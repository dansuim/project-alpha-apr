from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import TrackerLogin, TrackerSignup
from django.contrib.auth.models import User


# Create your views here.
# User Login
def user_login(request):
    if request.method == "POST":
        form = TrackerLogin(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = TrackerLogin()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# Log Out
def user_logout(request):
    logout(request)
    return redirect("login")


# Signup Form
def signup(request):
    if request.method == "POST":
        form = TrackerSignup(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                # Create a new user with those values
                user = User.objects.create_user(
                    username,
                    password=password,
                )

                # Login the user
                login(request, user)

                return redirect("list_projects")

            else:
                form.add_error("password", "the passwords do not match")

    else:
        form = TrackerSignup()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
